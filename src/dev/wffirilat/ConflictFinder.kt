package dev.wffirilat

import java.io.File

fun commonChildren(paths: List<File>): Map<File, List<File>> {
    val dirFiles = mutableMapOf<File, List<File>>()
    for  (dir in paths) {
        dirFiles[dir] = dir.listFiles()?.map { it.relativeTo(dir) } ?: emptyList()
    }
    val commonRoots = mutableMapOf<File, MutableList<File>>()
    for ((dir, files) in dirFiles) {
        for (file in files) {
            if (file in commonRoots) {
                commonRoots[file]?.add(dir)
            } else {
                commonRoots[file] = mutableListOf(dir)
            }
        }
    }
    return commonRoots.filter { it.value.size > 1 }
}

fun findConflicts(roots: List<File>, subpath: File = File("")): Map<List<File>, List<File>> {
    val paths: List<File> = roots.map { it.resolve(subpath) }.filter { it.exists() }
    if (paths.size <= 1) return emptyMap()

    if (paths.any { it.isFile }) {
        return mapOf(roots.map {it.resolve(subpath)}.filter { it.exists() } to listOf(subpath))
    }

    val conflicts = mutableMapOf<List<File>, MutableList<File>>()
    val children = commonChildren( roots.map {it.resolve(subpath)})

    for ((subdir, conflictroots) in children) {
        val subconflicts = findConflicts(conflictroots, subdir)
        for ((subroots, conflictpaths) in subconflicts) {
            val newRoots = subroots.map { it.parentFile }
            if (newRoots !in conflicts) conflicts[newRoots] = mutableListOf()
            for (path in conflictpaths) {
                conflicts[newRoots]?.add(subpath.resolve(path))
            }
        }
    }

    return conflicts
}

fun main() {
    val root1 = File("test/root1")
    val root2 = File("test/root2")
    val root3 = File("test/root3")

    val files = findConflicts(listOf(root1, root2, root3))

    for ((modules, conflicts) in files) {
        println("CONFLICTS BETWEEN:")
        for (m in modules) {
            println("\t$m")
        }
        println("________________________________________________")
        println()
        for (f in conflicts) {
            println(f);
        }
        println()
        println()
    }
}
